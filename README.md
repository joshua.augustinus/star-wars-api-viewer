### Task
https://www.notion.so/nicknack/GC-React-Developer-Challenge-2411950d194a4ebc9d20de235b531664


# Automated Testing
## Jest Unit Tests
Jest unit tests are more for the developer to help write functions. They are ideal for functions that have an input and return an output that can be checked. 

To perform Unit Tests:
```
yarn test
```

## Cypress E2E Tests
The bulk of testing user functionality is using E2E Tests. This is because UI is difficult to unit test. Unit tests are good at testing functions that have an input and return an output. A lot of UI is driven by events which is harder to unit test but can be tested using E2E test frameworks.

Cypress will not start the local server. So first:
```
yarn start
```

Next start Cypress:
```
yarn cypress
```

Then click the run button in top right.

### Cypress Videos
To get cypress videos run this command:
```
npx cypress run
```
The videos will be located inside cypress/videos