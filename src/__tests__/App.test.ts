import {
  convertResponseToPlanetsState,
  ensureHttps,
  getAllClimatePossibilities,
} from "apis/apiHelper";
import { PlanetsResponse } from "types";
import { removeValueFromCommaString, sortCommaString } from "util/arrayHelper";
import { getClimateCheckboxState } from "views/pages/Planet/climateHelper";
import {
  calculateStepSize,
  getSliderInfo,
  POPULATION_CONSTANTS,
} from "views/pages/Planet/planetHelper";
import { planetsResponse } from "../__testUtil__/planetsResponse";
import * as swApi from "../apis/swApi";
/**
 * To run tests, use this command:
 * yarn test
 */

const TIMEOUT = 10000;

it("converts planets response to PlanetsState", () => {
  const mockResponse: PlanetsResponse[] = planetsResponse;

  const result = convertResponseToPlanetsState(mockResponse);

  expect(result.count).toEqual(12);
  expect(result.numPages).toEqual(2);
  expect(result.pages.length).toEqual(2);
});

it("can get all possible climates based on the response", () => {
  const mockResponse: PlanetsResponse[] = planetsResponse;
  const result = getAllClimatePossibilities(mockResponse);
  expect(result.length).toEqual(5);
});

it("can parse climate checkbox state", () => {
  const allClimates = ["hot", "warm", "cold"];
  const planetClimate = "hot, cold";
  const result = getClimateCheckboxState(allClimates, planetClimate);

  expect(result[0]).toEqual({ climate: "hot", isChecked: true });
  expect(result[1]).toEqual({ climate: "warm", isChecked: false });
  expect(result[2]).toEqual({ climate: "cold", isChecked: true });
});

it("can remove an item from a comma seperated string", () => {
  const inputString = "tundra, ice caves, mountain ranges";
  const result = removeValueFromCommaString(inputString, "ice caves");
  expect(result).toEqual("tundra, mountain ranges");
});

it("can sort a comma seperated string", () => {
  const inputString = "tundra, ice caves, mountain ranges";
  const result = sortCommaString(inputString);
  expect(result).toEqual("ice caves, mountain ranges, tundra");

  const emptyResult = sortCommaString("");
  expect(emptyResult).toEqual("");
});

it("can convert http requests to https", () => {
  const inputString = "http://swapi.dev/api/planets/?page=2";
  const result = ensureHttps(inputString);
  expect(result).toEqual("https://swapi.dev/api/planets/?page=2");
});

it("can calculate slider step size based on population", () => {
  let result = calculateStepSize(11000);
  expect(result).toEqual(1000);
  result = calculateStepSize(500);
  expect(result).toEqual(100);
  result = calculateStepSize(0);
  expect(result).toEqual(1);
});

it("can calculate slider info", () => {
  let result = getSliderInfo("11000");
  expect(result).toEqual({ max: 22000, step: 1000 });
  result = getSliderInfo("unknown");
  expect(result).toEqual({
    max: POPULATION_CONSTANTS.assumedPopulationWhenUnknown * 2,
    step: 1000,
  });
  result = getSliderInfo("0");
  //When original pop is 0 then step will be 1
  expect(result).toEqual({
    max: POPULATION_CONSTANTS.sliderMaxLowest,
    step: 1,
  });

  result = getSliderInfo("100");
  //When original pop is 100 then step will be 10
  expect(result).toEqual({
    max: POPULATION_CONSTANTS.sliderMaxLowest,
    step: 10,
  });
});

it(
  "can call API",
  async () => {
    const result = await swApi.getAllPlanetsAsync();

    expect(result.count > 0).toEqual(true);
  },
  TIMEOUT
);
