import reducer from "./reducers";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { configureStore } from "@reduxjs/toolkit";

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["busy"],
};

const persistedReducer = persistReducer(persistConfig, reducer);

/**
 * This is how to setup the store using redux toolkit
 */
export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ immutableCheck: false, serializableCheck: false }),
});

/**
 * Used by PersistGate inside index.js
 */
export const persistor = persistStore(store);
