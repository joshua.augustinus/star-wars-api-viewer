import { createMuiTheme } from "@material-ui/core/styles";
import variables from "scss/_variables.scss";

/**
 * This theme is used by Material UI Components
 * In this project only the slider is from MUI
 * https://material-ui.com/customization/palette/
 */
export const theme = createMuiTheme({
  palette: {
    primary: {
      main: variables.primary,
    },
  },
});
