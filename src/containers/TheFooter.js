import React from "react";
import { CFooter } from "@coreui/react";

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a
          href="https://gitlab.com/joshua.augustinus/star-wars-api-viewer"
          target="_blank"
          rel="noopener noreferrer"
        >
          View Source Code
        </a>
        <span className="ml-1"> 2020 Joshua Augustinus</span>
      </div>
    </CFooter>
  );
};

export default React.memo(TheFooter);
