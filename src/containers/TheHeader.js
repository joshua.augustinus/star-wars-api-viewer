import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CBreadcrumbRouter,
} from "@coreui/react";

// routes config
import routes from "../routes";

import { TheHeaderDropdown } from "./index";
import { updateShowSidebar } from "reducers";
import { HeaderLogo } from "../assets/icons/HeaderLogo";
const TheHeader = () => {
  const dispatch = useDispatch();
  const sidebarShow = useSelector((state) => state.showSidebar);

  const toggleSidebar = () => {
    const val = [true, "responsive"].includes(sidebarShow)
      ? false
      : "responsive";
    dispatch(updateShowSidebar(val));
  };

  const toggleSidebarMobile = () => {
    const val = [false, "responsive"].includes(sidebarShow)
      ? true
      : "responsive";
    dispatch(updateShowSidebar(val));
  };

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        <HeaderLogo />
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavLink
          target="_blank"
          href="https://www.notion.so/GC-React-Developer-Challenge-2411950d194a4ebc9d20de235b531664"
        >
          Challenge
        </CHeaderNavLink>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink
            target="_blank"
            href="https://gitlab.com/joshua.augustinus/star-wars-api-viewer"
          >
            Source
          </CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink
            target="_blank"
            href="https://jauggy.atlassian.net/wiki/spaces/SWAV/overview"
          >
            Confluence
          </CHeaderNavLink>
        </CHeaderNavItem>
      </CHeaderNav>

      <CHeaderNav className="px-3">
        <TheHeaderDropdown />
      </CHeaderNav>

      <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter
          className="border-0 c-subheader-nav m-0 px-0 px-md-3"
          routes={routes}
        />
      </CSubheader>
    </CHeader>
  );
};

export default TheHeader;
