/**
 * Icons here:
 * https://coreui.io/docs/icons/icons-list/#coreui-icons-free-502-icons
 * You may need to add two entries inside src/assets/icons/index.js
 */
export default [
  {
    _tag: "CSidebarNavItem",
    name: "Planets",
    to: "/planets/0",
    icon: "cib-at-and-t",
  },
  {
    _tag: "CSidebarNavItem",
    name: "Misc",
    to: "/misc",
    icon: "cil-cog",
  },
];
