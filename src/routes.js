import React from "react";

const Planets = React.lazy(() => import("./views/pages/Planets"));
const Planet = React.lazy(() => import("./views/pages/Planet"));
const Misc = React.lazy(() => import("./views/pages/Misc"));
const routes = [
  { path: "/", exact: true, name: "" },

  {
    path: "/planets/:pageIndex",
    name: "Planets",
    component: Planets,
    exact: true,
  },
  {
    path: "/planets/:pageIndex/planet/:planetIndex",
    name: "Planet",
    component: Planet,
  },
  {
    path: "/misc",
    name: "Misc",
    component: Misc,
  },
];

export default routes;
