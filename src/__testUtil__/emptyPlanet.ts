import { Planet } from "types";

export const emptyPlanet: Planet = {
  name: "Empty Planet",

  climate: "arid",

  population: "0",

  url: "http://swapi.dev/api/planets/1/",
};
