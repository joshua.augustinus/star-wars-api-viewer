import { createAction, createReducer } from "@reduxjs/toolkit";
import {
  Planet,
  PlanetListPage,
  PlanetsState,
  RootState,
  SidebarState,
} from "types";

/**
 * Action creators
 */
export const updateIsBusy = createAction<boolean>("isBusy/update");
export const updatePlanetsState = createAction<PlanetsState>(
  "planetsState/update"
);

//This should only get called once per page
export const addPlanetListPage = createAction<PlanetListPage>(
  "planetListPage/add"
);
export const updateShowSidebar = createAction<SidebarState>(
  "showSidebar/update"
);
export const updatePlanet = createAction<Planet>("planet/update");
//reset the rootstate
export const deleteRootState = createAction("rootState/delete");
export const updatePlanetsSubheading = createAction<string>(
  "planetsSubheading/update"
);

/**
 * Initial state of reducer
 */
const initialPlanetsState: PlanetsState = {
  count: 0,
  numPages: 0,
  pages: [],
  climateTypes: [],
  subHeading: "",
};
const initialState: RootState = {
  isBusy: false,
  planetsState: initialPlanetsState,
  showSidebar: "responsive",
};

/**
 * The rootReducer
 * This uses immer.
 * https://jauggy.atlassian.net/wiki/spaces/REAC/pages/79560705/Redux-toolkit
 */
const rootReducer = createReducer(initialState, (builder) => {
  builder.addCase(deleteRootState, (state, action) => {
    console.log("initial state", initialState);
    //Immer: do not modify the WHOLE state object like this:
    //state = initialState
    //Instead return a new state object
    return initialState;
  });

  builder.addCase(updateIsBusy, (state, action) => {
    state.isBusy = action.payload;
  });

  builder.addCase(updateShowSidebar, (state, action) => {
    state.showSidebar = action.payload;
  });

  builder.addCase(updatePlanetsState, (state, action) => {
    state.planetsState = action.payload;
  });

  builder.addCase(addPlanetListPage, (state, action) => {
    //create a new array with the new page
    let newArray = [];
    for (let i = 0; i < state.planetsState.pages.length; i++) {
      if (action.payload.pageNumber - 1 === i) {
        newArray.push(action.payload);
      } else {
        newArray.push(state.planetsState.pages[i]);
      }
    }

    state.planetsState.pages = newArray;
  });

  builder.addCase(updatePlanet, (state, action) => {
    const pageIndex = action.payload.pageIndex;
    const planetIndex = action.payload.index;
    state.planetsState.pages[pageIndex].data[planetIndex] = action.payload;
  });

  builder.addCase(updatePlanetsSubheading, (state, action) => {
    state.planetsState.subHeading = action.payload;
  });
});

export default rootReducer;
