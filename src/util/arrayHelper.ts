/**
 * Split comma seperated string to array
 * Trims white space
 */
export const splitStringToArray = (commaSeperatedString: string): string[] => {
  let result = commaSeperatedString.split(",").map(function (item) {
    return item.trim();
  });

  return result;
};

export const removeValueFromCommaString = (
  commaSeperatedString: string,
  removeValue: string
) => {
  const separator = ", ";
  var values = commaSeperatedString.split(separator);
  for (var i = 0; i < values.length; i++) {
    if (values[i] === removeValue) {
      values.splice(i, 1);
      return values.join(separator);
    }
  }

  //Didn't do anything so return the original
  return commaSeperatedString;
};

export const addValueToCommaString = (
  commaSeperatedString: string,
  addValue: string
) => {
  if (commaSeperatedString.length > 0)
    return commaSeperatedString + ", " + addValue;
  else return addValue;
};

export const sortCommaString = (commaSeperatedString: string) => {
  const separator = ", ";
  const tempArray = splitStringToArray(commaSeperatedString);
  tempArray.sort();
  return tempArray.join(separator);
};
