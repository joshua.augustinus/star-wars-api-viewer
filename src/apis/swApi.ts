import { PlanetsResponse, PlanetsState } from "types";
import { convertResponseToPlanetsState } from "./apiHelper";
import * as baseApi from "./baseApi";

const baseUrl = "https://swapi.dev/api";
function getAsync(route: string) {
  return baseApi.getAsync(route, baseUrl);
}

/**
 * Gets the first page then gets all future pages
 */
async function getAllPlanetsFromApiAsync(): Promise<PlanetsResponse[]> {
  let hasMore = true;
  let pageNumber = 1;
  let result: PlanetsResponse[] = [];

  //if there is next go again
  while (hasMore) {
    let response = (await getAsync(
      "planets/?page=" + pageNumber
    )) as PlanetsResponse;

    hasMore = response.next !== null;
    pageNumber++;
    result.push(response);
  }

  return result;
}

export async function getAllPlanetsAsync(): Promise<PlanetsState> {
  let response = await getAllPlanetsFromApiAsync();
  //convert response to state
  let state = convertResponseToPlanetsState(response);
  return state;
}
