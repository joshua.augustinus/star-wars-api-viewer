import { PlanetListPage, PlanetsResponse, PlanetsState } from "types";

/**
 * Converts the initial call to getPlanets to a state object
 */
export function convertResponseToPlanetsState(
  response: PlanetsResponse[]
): PlanetsState {
  if (response.length > 0) {
    let resultsPerPage = response[0].results.length;
    var result: PlanetsState = {
      count: response[0].count,
      numPages: Math.ceil(response[0].count / resultsPerPage),
      pages: [],
      climateTypes: getAllClimatePossibilities(response),
      subHeading: "",
    };

    for (let i = 1; i <= result.numPages; i++) {
      let page = toPlanetPage(response[i - 1], i);

      result.pages.push(page);
    }

    return result;
  } else {
    var emptyResult: PlanetsState = {
      count: 0,
      numPages: 0,
      pages: [],
      climateTypes: [],
      subHeading: "",
    };

    return emptyResult;
  }
}

export function toPlanetPage(
  response: PlanetsResponse,
  pageNumber: number
): PlanetListPage {
  //add index to planets
  for (let i = 0; i < response.results.length; i++) {
    response.results[i].index = i;
    response.results[i].pageIndex = pageNumber - 1;
  }
  const page = {
    pageNumber: pageNumber,
    data: response.results,
  };

  return page;
}

/**
 * This function shouldn't need to exist. The API should have an endpoint to get all possible climates
 */
export function getAllClimatePossibilities(
  response: PlanetsResponse[]
): string[] {
  let allClimates: string[] = [];
  for (let i = 0; i < response.length; i++) {
    let planetResponse = response[i];

    for (let j = 0; j < planetResponse.results.length; j++) {
      let planet = planetResponse.results[j];
      let climateArray = planet.climate.split(",").map(function (item) {
        return item.trim();
      });
      allClimates = allClimates.concat(climateArray);
    }
  }

  var uniqueClimates = allClimates.filter(function (item, pos) {
    return allClimates.indexOf(item) === pos;
  });

  return uniqueClimates.sort();
}

export function ensureHttps(url: string) {
  var res = url.replace("http://", "https://");
  return res;
}
