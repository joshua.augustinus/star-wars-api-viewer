import React from "react";

import { CFormGroup, CInputCheckbox, CLabel } from "@coreui/react";
import { ClimateCheckboxState } from "types";
import { getClimateCheckboxState } from "./climateHelper";

interface Props {
  allClimates: string[];
  /**
   * Comma seperated string
   */
  planetClimateString: string;
  checkChanged: (climateType: string, isChecked: boolean) => void;
}

const ClimateFormGroup = React.memo((props: Props) => {
  console.log("Rendered ClimateFormGroup");
  //Unfortunately this array will always be "new" causing ClimateFormGroup to re-render
  //even if it is using React.useMemo
  const climateStateArray = getClimateCheckboxState(
    props.allClimates,
    props.planetClimateString
  );

  return (
    <CFormGroup>
      <CLabel>Climates</CLabel>
      {climateStateArray.map((item: ClimateCheckboxState, index: number) => {
        return (
          <CFormGroup
            variant="checkbox"
            className="checkbox"
            key={item.climate}
          >
            <CInputCheckbox
              id={item.climate}
              name={item.climate}
              value={item.climate}
              checked={item.isChecked}
              onChange={(cb: any) =>
                props.checkChanged(item.climate, cb.target.checked)
              }
            />
            <CLabel
              variant="checkbox"
              className="form-check-label"
              htmlFor={item.climate}
            >
              {item.climate}
            </CLabel>
          </CFormGroup>
        );
      })}
    </CFormGroup>
  );
});

export default ClimateFormGroup;
