import { PopulationNumber } from "types";

export const POPULATION_CONSTANTS = {
  sliderMaxLowest: 1000,
  assumedPopulationWhenUnknown: 500000000000,
};

interface SliderInfo {
  max: number;
  step: number;
}

export const getSliderInfo = (populationString: string): SliderInfo => {
  console.log("getSliderMax called");

  let population = parseInt(populationString);
  if (isNaN(population)) {
    population = POPULATION_CONSTANTS.assumedPopulationWhenUnknown;
  }

  let max = Math.max(population * 2, POPULATION_CONSTANTS.sliderMaxLowest);
  let step = calculateStepSize(population);
  return { max: max, step: step };
};

export const getPopulationNumber = (
  populationString: string
): PopulationNumber => {
  const population = parseInt(populationString);
  if (isNaN(population)) {
    return "unknown";
  } else {
    return population;
  }
};

export const calculateStepSize = (originalPopulation: number) => {
  let step = 1000;
  const MIN_STEP = 1;
  if (originalPopulation <= 0) return MIN_STEP;

  while (step > 0) {
    if (originalPopulation % step === 0 && originalPopulation / step !== 1)
      return step;
    else step = step / 10;
  }

  return MIN_STEP;
};
