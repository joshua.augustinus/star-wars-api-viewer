import { ClimateCheckboxState } from "types";
import { splitStringToArray } from "util/arrayHelper";

/**
 * Gets the state of checkboxes for climate
 * @param allClimateTypes Example: ['hot', 'cold', 'wet']
 * @param planetClimate Example: 'hot', 'wet'
 * @returns Example: [{climate:'hot', isChecked:true},...]
 */
export const getClimateCheckboxState = (
  allClimateTypes: string[],
  planetClimate: string
): ClimateCheckboxState[] => {
  let result: ClimateCheckboxState[] = [];
  const planetClimateArray = splitStringToArray(planetClimate);
  for (let i = 0; i < allClimateTypes.length; i++) {
    let climateType = allClimateTypes[i];
    result.push({
      climate: climateType,
      isChecked: planetClimateArray.includes(climateType),
    });
  }
  return result;
};
