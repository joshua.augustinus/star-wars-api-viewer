import React, { useState } from "react";
import { Card, CardBody, CardHeader } from "../../components/Card";
import { useParams } from "react-router-dom";
import { CButton, CFormGroup, CInput, CLabel } from "@coreui/react";
import { useDispatch, useSelector } from "react-redux";
import { Planet, PlanetRouteParams, RootState } from "types";
import ClimateFormGroup from "./ClimateFormGroup";
import {
  addValueToCommaString,
  removeValueFromCommaString,
  sortCommaString,
} from "util/arrayHelper";
import { updatePlanet } from "reducers";
import { PopulationFormGroup } from "./PopulationFormGroup";

const cancel = () => {
  window.history.back();
};

const UNKNOWN_CLIMATE = "unknown";
const CPlanet = () => {
  const dispatch = useDispatch();

  const { planetIndex, pageIndex } = useParams<PlanetRouteParams>();
  /**
   * Be careful. Planets pages start at index 1. This is to be consisted with the API
   * But planet indexes start at index 0
   */
  const planet = useSelector(
    (state: RootState) =>
      state.planetsState.pages[parseInt(pageIndex)].data[parseInt(planetIndex)]
  );
  const [population, setPopulation] = useState(planet.population);
  const [planetClimateString, setPlanetClimateString] = useState(
    planet.climate
  );

  const [planetNameInput, setPlanetNameInput] = useState(planet.name);
  const allClimates = useSelector(
    (state: RootState) => state.planetsState.climateTypes
  );

  /**
   * Handler that gets called when saving changes
   */
  const saveChanges = () => {
    let newPlanet: Planet = {
      ...planet,
      climate: sortCommaString(planetClimateString),
      name: planetNameInput,
      population: population,
    };

    //save to state
    dispatch(updatePlanet(newPlanet));

    //go back
    window.history.back();
  };

  const populationChanged = (value: string) => {
    setPopulation(value);
  };

  /**
   * Handle checkbox changes
   */
  const climateCheckChanged = React.useCallback(
    (climateType: string, isChecked: boolean) => {
      let newClimateString = "";
      if (isChecked) {
        if (climateType === UNKNOWN_CLIMATE) {
          //Unknown checked so uncheck everything else
          newClimateString = UNKNOWN_CLIMATE;
        } else {
          //Make sure unknown is unchecked when something else is checked
          let tempClimateString = removeValueFromCommaString(
            planetClimateString,
            UNKNOWN_CLIMATE
          );

          newClimateString = addValueToCommaString(
            tempClimateString,
            climateType
          );
        }
      } else {
        newClimateString = removeValueFromCommaString(
          planetClimateString,
          climateType
        );
        setPlanetClimateString(newClimateString);
      }
      setPlanetClimateString(newClimateString);
    },
    [planetClimateString]
  );

  return (
    <Card>
      <CardHeader>{planet.name}</CardHeader>
      <CardBody>
        <CFormGroup>
          <CLabel htmlFor="planetName">Planet Name</CLabel>
          <CInput
            data-cy="input-planet-name"
            type="text"
            id="planetName"
            name="planetName"
            maxLength={10}
            value={planetNameInput}
            onChange={(e) => setPlanetNameInput(e.currentTarget.value)}
          />
        </CFormGroup>

        <PopulationFormGroup
          value={population}
          onChange={populationChanged}
          originalPopulation={planet.population}
        />

        <ClimateFormGroup
          allClimates={allClimates}
          planetClimateString={planetClimateString}
          checkChanged={climateCheckChanged}
        />

        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <CButton onClick={saveChanges} color="primary">
            Save Changes
          </CButton>
          <CButton onClick={cancel} color="primary" variant="outline">
            Cancel
          </CButton>
        </div>
      </CardBody>
    </Card>
  );
};

export default CPlanet;
