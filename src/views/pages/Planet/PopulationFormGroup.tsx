import React, { useState } from "react";
import { CFormGroup, CLabel, CSwitch } from "@coreui/react";
import Slider from "@material-ui/core/Slider";
import { getPopulationNumber, getSliderInfo } from "./planetHelper";
import { PopulationNumber } from "types";

interface Props {
  originalPopulation: string;
  onChange: (newPopulation: string) => void;
  value: string;
}

const PopulationFormGroup = (props: Props) => {
  const populationNumber: PopulationNumber = getPopulationNumber(props.value);
  const sliderInfo = getSliderInfo(props.originalPopulation);
  const [hiddenSliderValue, setHiddenSliderValue] = useState(
    sliderInfo.max / 2
  );

  const sliderChanged = (e: any, value: number) => {
    props.onChange(value.toString());
  };
  const popIsNumber = populationNumber !== "unknown";

  const switchChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log("switch changed", e.target.value);
    if (popIsNumber) {
      setHiddenSliderValue(populationNumber as number);
      props.onChange("unknown");
    } else {
      props.onChange(hiddenSliderValue.toString());
    }
  };

  return (
    <CFormGroup>
      <div className="d-flex">
        <div style={{ width: 240 }}>
          <CLabel htmlFor="population" data-cy="label-population">
            Population: {populationNumber.toLocaleString()}
          </CLabel>
        </div>

        <CSwitch
          data-cy="switch-population"
          variant="3d"
          color="primary"
          checked={popIsNumber}
          onChange={switchChanged}
        />
      </div>

      {popIsNumber && (
        <Slider
          step={sliderInfo.step}
          name="population"
          id="population"
          value={populationNumber as number}
          onChange={sliderChanged}
          aria-labelledby="discrete-slider"
          min={0}
          max={sliderInfo.max}
        />
      )}
    </CFormGroup>
  );
};

export { PopulationFormGroup };
