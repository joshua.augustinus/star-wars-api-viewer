import React, { useEffect } from "react";
import { CCardBody } from "@coreui/react";
import { Card, CardHeader } from "../../components/Card";
import { RootState } from "types";
import { useDispatch, useSelector } from "react-redux";
import * as swApi from "../../../apis/swApi";
import { useLocation } from "react-router-dom";
import { ActivityIndicator } from "views/components/ActivityIndicator";
/* eslint-disable */
import PlanetsHeader from "./PlanetsHeader";
import { updatePlanetsState } from "reducers";
import FilteredPlanetList from "./FilteredPlanetList";
import UnfilteredPlanetList from "./UnfilteredPlanetList";
import queryString from "query-string";

const TITLE = "Planets";

const Planets = (props: any) => {
  const numPages = useSelector(
    (state: RootState) => state.planetsState.numPages
  );
  const planetCount = useSelector(
    (state: RootState) => state.planetsState.count
  );
  const dispatch = useDispatch();
  const location = useLocation();

  const isBusy = useSelector((state: RootState) => state.isBusy);

  const filter = queryString.parse(location.search).name as string;

  useEffect(() => {
    if (planetCount === 0) {
      swApi.getAllPlanetsAsync().then((state) => {
        dispatch(updatePlanetsState(state));
      });
    }
  }, [planetCount]);

  if (numPages === 0 || isBusy) {
    return (
      <Card>
        <CardHeader>{TITLE}</CardHeader>
        <CCardBody>
          <ActivityIndicator />
        </CCardBody>
      </Card>
    );
  }

  const content = filter ? (
    <FilteredPlanetList nameFilter={filter} />
  ) : (
    <UnfilteredPlanetList />
  );

  return (
    <Card>
      <CardHeader>
        <PlanetsHeader title={TITLE} nameFilter={filter} />
      </CardHeader>
      <CCardBody>{content}</CCardBody>
    </Card>
  );
};

export default Planets;
