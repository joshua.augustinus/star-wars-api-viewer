import React from "react";
import { CButton } from "@coreui/react";
import { Planet } from "types";
import { useHistory } from "react-router-dom";

interface Props {
  /**
   * The name to search for
   */
  planet: Planet;
}

const formatNumber = (population: string) => {
  const popNumber = parseInt(population);
  if (isNaN(popNumber)) return population;
  else return popNumber.toLocaleString();
};

const PlanetButton = React.memo((props: Props) => {
  const history = useHistory();

  const goToPlanetDetail = () => {
    const pageIndex: number = props.planet.pageIndex;
    history.push("/planets/" + pageIndex + "/planet/" + props.planet.index);
  };

  return (
    <CButton
      onClick={goToPlanetDetail}
      block
      color="light"
      style={style.planetButton}
    >
      <div>Name: {props.planet.name}</div>
      <div>Population: {formatNumber(props.planet.population)}</div>
      <div>Climate: {props.planet.climate}</div>
    </CButton>
  );
});

export default PlanetButton;

const style: any = {
  planetButton: {
    textAlign: "left",
  },
};
