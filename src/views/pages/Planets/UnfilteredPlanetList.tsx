import React from "react";
import { CPagination } from "@coreui/react";
import { Planet, RootState } from "types";
import { useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import PlanetButton from "./PlanetButton";

const UnfilteredPlanetList = () => {
  const numPages = useSelector(
    (state: RootState) => state.planetsState.numPages
  );

  const { pageIndex } = useParams<any>();
  const pageIndexNum = parseInt(pageIndex);
  const currentPage = useSelector(
    (state: RootState) => state.planetsState.pages[pageIndexNum]
  );

  const history = useHistory();

  const setActivePage = (pageNumber: number) => {
    const pageIndex = pageNumber - 1;
    history.push("/planets/" + pageIndex);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  return (
    <>
      {currentPage.data.map((item: Planet) => {
        return <PlanetButton key={item.url} planet={item} />;
      })}
      <div style={{ marginTop: 10 }}>
        <CPagination
          limit={10}
          dots={false}
          arrows={false}
          data-cy="pagination"
          activePage={currentPage.pageNumber}
          pages={numPages}
          onActivePageChange={(i: number) => setActivePage(i)}
        ></CPagination>
      </div>
    </>
  );
};

export default UnfilteredPlanetList;
