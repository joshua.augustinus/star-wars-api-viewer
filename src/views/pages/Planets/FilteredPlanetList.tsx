/* eslint-disable */
import React, { useEffect } from "react";
import { Planet, RootState } from "types";
import { useDispatch, useSelector } from "react-redux";
import { combinePlanetListPages } from "./planetsHelper";
import PlanetButton from "./PlanetButton";
import { updatePlanetsSubheading } from "reducers";

interface Props {
  /**
   * The name to search for
   */
  nameFilter: string;
}
const FilteredPlanetList = React.memo((props: Props) => {
  const pages = useSelector((state: RootState) => state.planetsState.pages);
  const dispatch = useDispatch();
  const combined = combinePlanetListPages(pages, props.nameFilter);
  useEffect(() => {
    dispatch(updatePlanetsSubheading(combined.data.length + " results"));

    return () => {
      dispatch(updatePlanetsSubheading(""));
    };
  }, [props.nameFilter]);

  return (
    <>
      {combined.data.map((item: Planet) => {
        return <PlanetButton key={item.url} planet={item} />;
      })}
    </>
  );
});

export default FilteredPlanetList;
