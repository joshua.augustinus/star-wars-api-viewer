import React, { useState } from "react";
import { CButton, CCol, CInput, CRow } from "@coreui/react";
import { useHistory } from "react-router-dom";
import { RootState } from "types";
import { useSelector } from "react-redux";

interface Props {
  title: string;
  nameFilter: string;
}

const PlanetsHeader = (props: Props) => {
  const subHeading = useSelector(
    (state: RootState) => state.planetsState.subHeading
  );
  const history = useHistory();
  const [filterValue, setFilterValue] = useState(
    props.nameFilter ? props.nameFilter : ""
  );
  const planetCount = useSelector(
    (state: RootState) => state.planetsState.count
  );

  const subHeadingText =
    subHeading.length > 0 ? subHeading : planetCount + " results";

  const onFilter = (e: any) => {
    e.preventDefault();
    if (filterValue.length === 0) {
      history.push("/planets/0");
    } else {
      history.push("/planets/0?name=" + filterValue);
    }
  };

  const inputChanged = (event: React.FormEvent<HTMLInputElement>) => {
    setFilterValue(event.currentTarget.value);
  };

  return (
    <form onSubmit={onFilter}>
      <CRow>
        <CCol xs="3">
          <div>{props.title}</div>
          <small>{subHeadingText}</small>
        </CCol>
        <CCol xs="7">
          <CInput
            data-cy="planet-filter"
            placeholder="Enter planet name"
            type="text"
            maxLength={10}
            value={filterValue}
            onChange={inputChanged}
          />
        </CCol>
        <CCol
          xs="2"
          style={{
            padding: 0,
          }}
        >
          <CButton type="submit" color="primary" variant="outline">
            Filter
          </CButton>
        </CCol>
      </CRow>
    </form>
  );
};

export default PlanetsHeader;
