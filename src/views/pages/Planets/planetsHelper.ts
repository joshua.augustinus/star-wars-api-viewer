import { PlanetListPage } from "types";

export function combinePlanetListPages(
  pages: PlanetListPage[],
  nameFilter: string
): PlanetListPage {
  let result: PlanetListPage = { pageNumber: 1, data: [] };

  for (let i = 0; i < pages.length; i++) {
    let filtered = pages[i].data.filter(
      (item) => item.name.toLowerCase().indexOf(nameFilter.toLowerCase()) >= 0
    );
    result.data = result.data.concat(filtered);
  }

  return result;
}
