import React from "react";
import { CButton, CCardBody } from "@coreui/react";
import { Card, CardHeader } from "../../components/Card";
import { deleteRootState } from "reducers";
import { useDispatch } from "react-redux";
import { showNotificationInfo } from "util/notificationHelper";

const Misc = (props: any) => {
  const dispatch = useDispatch();
  const resetStore = () => {
    dispatch(deleteRootState());
    showNotificationInfo("Saved changes reset");
  };

  return (
    <Card>
      <CardHeader>Misc</CardHeader>
      <CCardBody>
        <CButton
          onClick={resetStore}
          color="primary"
          style={{ textAlign: "left" }}
        >
          Reset Changes
        </CButton>
      </CCardBody>
    </Card>
  );
};

export default Misc;
