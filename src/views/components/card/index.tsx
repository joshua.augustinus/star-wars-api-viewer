import React from "react";
import { CCard, CCardBody, CCardHeader } from "@coreui/react";

const Card = (props: any) => {
  return <CCard style={styles.card}>{props.children}</CCard>;
};

export { Card };

const CardHeader = (props: any) => {
  return <CCardHeader style={styles.cardHeader}>{props.children}</CCardHeader>;
};

export { CardHeader };

const CardBody = (props: any) => {
  return <CCardBody>{props.children}</CCardBody>;
};

export { CardBody };

const styles = {
  cardHeader: { borderTopLeftRadius: 10, borderTopRightRadius: 10 },
  card: { borderRadius: 10 },
};
