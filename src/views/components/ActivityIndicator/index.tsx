import React from "react";

const ActivityIndicator = (props: any) => {
  return (
    <div className="sk-flow custom-spinner sk-center">
      <div className="sk-flow-dot"></div>
      <div className="sk-flow-dot"></div>
      <div className="sk-flow-dot"></div>
    </div>
  );
};

export { ActivityIndicator };
