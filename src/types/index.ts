import { HttpResponse } from "@microsoft/signalr";
import { Session } from "@opentok/client";

/**
 * Required for style sheets
 * */
export interface ReactStyleSheet {
  [key: string]: React.CSSProperties;
}

/**
 * Reducer States
 */
export interface RootState {
  isBusy: boolean;
  planetsState: PlanetsState;
  showSidebar: SidebarState;
}

export interface PlanetsResponse extends HttpResponse {
  count: number;
  next: string;
  previous: string;
  results: Planet[];
}

export interface Planet {
  name: string;
  population: string;
  climate: string;
  /**
   * This should be used like a key as it is unique
   */
  url: string;
  /**
   * You can find Planet inside the state using both these indexes
   */
  index?: number;
  pageIndex?: number;
}

/**
 * This page contains multiple planets
 */
export interface PlanetListPage {
  pageNumber: number;
  data: Planet[];
}

export interface PlanetsState {
  count: number;
  numPages: number;
  pages: PlanetListPage[];
  climateTypes: string[];
  subHeading: string;
}

export type SidebarState = true | false | "responsive";

export interface ClimateCheckboxState {
  climate: string;
  isChecked: boolean;
}

export type PopulationNumber = "unknown" | number;

export interface PlanetRouteParams {
  pageIndex?: string;
  planetIndex?: string;
}
