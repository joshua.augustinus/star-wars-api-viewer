import {
  click,
  clickText,
  clickTextWithin,
  expectText,
  expectTextNotExists,
  get,
  getText,
  inputHasValue,
  inputNotHaveValue,
  screenshot,
  scrollToBottom,
  slideMuiRelative,
  slideMuiTo,
  type,
} from "../util/e2eUtil";
import { ViewPortType } from "../util/types";

/**
 * This class is called from cypress/integration/*.ts
 * @param size ViewPort size
 */
const tests = (size: ViewPortType, screenshotPrefix: string) => {
  before(() => {});

  beforeEach(() => {
    if (Cypress._.isArray(size)) {
      cy.viewport(size[0], size[1]);
    } else {
      cy.viewport(size);
    }
    cy.visit("http://localhost:3000/#/");
    expectText("Tatooine");
  });

  it("has pagination", () => {
    scrollToBottom();
    clickTextWithin("2", "pagination");
    expectText("Geonosis");
  });

  it("can filter by planet name", () => {
    screenshot("Unfiltered Planets");
    expectTextNotExists("Geonosis");
    type("geo", "planet-filter");
    clickText("Filter");
    expectText("Geonosis");
    screenshot("Filtered Planets");
  });

  it("can edit a planet's name", () => {
    clickText("Tatooine");
    expectText("Save Changes");
    screenshot("Planet Form");
    //Test for max length
    type("12345678901", "input-planet-name");
    inputHasValue("1234567890", "input-planet-name");
    inputNotHaveValue("12345678901", "input-planet-name");

    type("Geonosis", "input-planet-name");
    clickText("Save Changes");
    expectText("Geonosis");
    type("geo", "planet-filter");
    clickText("Filter");

    expectText("Geonosis");
    expectText("2 results");
  });

  it("can navigate to correct planet after filter", () => {
    //filter for eriadu
    //click Eriadu
    //wait for 'Save Changes'
    //check that Eriadu exists
    type("eri", "planet-filter");
    clickText("Filter");
    expectText("Eriadu");
    clickText("Eriadu");
    expectText("Save Changes");
    expectText("Eriadu");
  });

  it("can change population via slider", () => {
    clickText("Tatooine");
    expectText("Population: 200,000");
    slideMuiRelative(100);
    getText("label-population").then((text) => {
      clickText("Save Changes");
      expectText("Filter");
      expectText(text);
    });
  });

  it("can edit planets with unknown population", () => {
    type("aleen", "planet-filter");
    clickText("Filter");
    clickText("Aleen Minor");
    expectText("Population: unknown");

    //change population
    click("switch-population", true);
    expectText("Population: 500,000,000,000");
    slideMuiRelative(-100);
    expectTextNotExists("Population: 500,000,000,000");
    getText("label-population").then((text) => {
      clickText("Save Changes");
      expectText("Filter");
      expectText(text);
    });
  });

  it("can edit planets with 0 population", () => {
    type("aleen", "planet-filter");
    clickText("Filter");
    clickText("Aleen Minor");
    expectText("Population: unknown");

    //change population to zero
    click("switch-population", true);
    expectText("Population: 500,000,000,000");
    slideMuiRelative(-10000);
    expectText("Population: 0");
    clickText("Save Changes");
    expectText("Filter");
    clickText("Aleen Minor");
    expectText("Save Changes");

    //change population to non zero
    expectText("Population: 0");
    slideMuiRelative(10000);
    expectTextNotExists("Population: 0");
    clickText("Save Changes");
    expectText("Filter");
    expectTextNotExists("Population: 0");
  });

  it("can change climate", () => {
    type("eri", "planet-filter");
    clickText("Filter");
    clickText("Eriadu");
    clickText("arid");
    clickText("Save Changes");
    expectText("Climate: arid, polluted");
    clickText("Eriadu");
    clickText("polluted");
    clickText("Save Changes");
    expectText("Climate: arid");

    //Check handling of unknown
    clickText("Eriadu");
    clickText("unknown");
    clickText("Save Changes");
    expectText("Climate: unknown");

    //Unknown auto unselected when somethng else selected
    clickText("Eriadu");
    clickText("hot");
    clickText("rocky");
    clickText("artic");
    clickText("Save Changes");
    expectText("Climate: artic, hot, rocky");
  });

  it("can reset planets filter", () => {
    type("eri", "planet-filter");
    clickText("Filter");
    type("", "planet-filter");
    clickText("Filter");
    expectText("60 results");
  });
};

export default tests;
