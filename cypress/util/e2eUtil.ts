const options = { timeout: 10000 };
export function type(text: string, selector: string) {
  if (text.length === 0) {
    cy.get("[data-cy=" + selector + "]")
      .clear()

      .blur();
  } else {
    cy.get("[data-cy=" + selector + "]")
      .clear()
      .type(text)
      .blur();
  }
}

/**
 * See examples of using data-cy selector
 * https://docs.cypress.io/guides/references/best-practices.html
 */
export function get(selector: string) {
  return cy.get("[data-cy=" + selector + "]");
}

export function clickText(text: string) {
  cy.contains(text).click();
}

/**
 * Clicks an element using data-cy selector
 * @param selector
 * @param force Defaults to false; Set to true if cypress fails to click the thing without forcing
 */
export function click(selector: string, force: boolean = false) {
  cy.get("[data-cy=" + selector + "]").click({ force: force });
}

/**
 * This will not work for input text boxes
 * Use inputHasValue instead
 */
export function expectText(text: string) {
  cy.contains(text, options).should("be.visible");
}

export function expectTextNotExists(text: string) {
  cy.contains(text).should("not.be.visible");
}

/**
 * Screenshots will by auto saved to subfolder when running
 * npx cypress run
 * but not
 * yarn cypress
 */
export function screenshot(name: string) {
  cy.wait(500);
  cy.screenshot(name);
}

export function scrollToBottom() {
  cy.scrollTo("bottom");
}

export function inputHasValue(value: string, selector: string) {
  cy.get("[data-cy=" + selector + "]").should("have.value", value);
}

export function inputNotHaveValue(value: string, selector: string) {
  cy.get("[data-cy=" + selector + "]").should("not.have.value", value);
}

/**
 * @param text The text to click
 * @param selector a data-cy selector
 */
export function clickTextWithin(text: string, selector: string) {
  cy.get("[data-cy=" + selector + "]").within(() => {
    cy.contains(text).click(); // Only yield inputs within form
  });
}

/**
 * See here:
 * https://stackoverflow.com/questions/6073505/what-is-the-difference-between-screenx-y-clientx-y-and-pagex-y
 * for information about clientX
 */
export function slideMuiTo(clientX: number) {
  //cy.get("span[data-cy=" + selector + "]")
  cy.get(".MuiSlider-thumb")
    .focus()
    .trigger("mousedown")
    .trigger("mousemove", { which: 1, clientX: clientX })
    .trigger("mouseup");
}

/**
 * Slide material UI slider x pixels left or right
 * @param x number of pixels to slide. negative is left
 */
export function slideMuiRelative(x: number) {
  cy.get(".MuiSlider-thumb").then(($el) => {
    const bounds = $el[0].getBoundingClientRect();
    cy.get(".MuiSlider-thumb")
      .focus()
      .trigger("mousedown")
      .trigger("mousemove", { which: 1, clientX: bounds.left + x })
      .trigger("mouseup");
  });
}

/**
 * To use .then() on the result of this function
 */
export function getText(selector: string) {
  return cy.get("[data-cy=" + selector + "]").invoke("text");
}
